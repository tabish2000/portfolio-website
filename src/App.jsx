import React from "react";
import Header from './components/header/Header'
import Nav from './components/nav/Nav'
import Footer from './components/footer/Footer'
import About from './components/about/About'
import Contact from './components/contact/Cotact'
import Portfolio from './components/portfolio/Portfolio'
import Experience from "./components/experience/Experience";

const App = () => {
    return(
       <>
       <Header/>
       <About/>
       <Experience/>
       <Portfolio/> 
       <Nav/> 
       <Contact/>
       <Footer/>
       
       </>  
    )
}

export default App