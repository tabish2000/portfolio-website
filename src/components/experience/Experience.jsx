import React from 'react'
import './experience.css'
import {ImHtmlFive} from 'react-icons/im'
import {TfiCss3} from 'react-icons/tfi'
import{IoLogoJavascript} from 'react-icons/io'
import {SiTailwindcss} from 'react-icons/si'
import {GrReactjs} from 'react-icons/gr'
import {TbBrandRedux} from 'react-icons/tb'
import {FaNodeJs} from 'react-icons/fa'
import{DiMongodb} from 'react-icons/di'
import {GrMysql} from 'react-icons/gr'
import {SiExpress} from 'react-icons/si'


function Experience() {
  return (
   <section id='experience'>
        <h5>What Skills I Have</h5>
        <h2>My Experience</h2>

        <div className='container experience__container'>

            <div className='experience__frontend'>

                <h3>Frontend Development</h3>

                <div className='experience__content'>

                    

                    <article className='experience__details'>
                        <ImHtmlFive className='experience__details-icon' />
                        <div>
                        <h4>HTML</h4>
                        <small className='text-light'>Intermediate</small>
                        </div>
                    </article>

                    <article className='experience__details'>
                        <TfiCss3 className='experience__details-icon'  />
                       <div>
                       <h4>CSS</h4>
                        <small className='text-light'>Intermediate</small>
                       </div>
                    </article>
                    
                    <article className='experience__details'>
                        <IoLogoJavascript  className='experience__details-icon'/>
                         <div>
                         <h4>JavaScript</h4>
                        <small className='text-light'>Intermediate</small>
                         </div>
                    </article>

                    <article className='experience__details'>
                        <SiTailwindcss className='experience__details-icon'/>
                        <div>
                        <h4>Tailwind</h4>
                        <small className='text-light'>Beginner</small>
                        </div>
                    </article>

                    <article className='experience__details'>
                        <GrReactjs className='experience__details-icon'/>
                         <div>
                         <h4>React</h4>
                        <small className='text-light'>Intermediate</small>
                         </div>
                    </article>

                    <article className='experience__details'>
                        <div>
                        <TbBrandRedux className='experience__details-icon'/>
                        <h4>Redux</h4>
                        <small className='text-light'>Intermediate</small>
                        </div>
                    </article>

                    

                </div>

            </div>

        <div className='experience__backend'>

                <h3>Backend  Development</h3>

            <div className='experience__content'>

                <article className='experience__details'>
                    <FaNodeJs className='experience__details-icon' />
                        <h4>Node JS</h4>
                        <small className='text-light'>Intermediate</small>
                </article>

                <article className='experience__details'>
                    <DiMongodb className='experience__details-icon'/>
                    <h4>MongoDB</h4>
                    <small className='text-light'>Intermediate</small>
                </article>

                <article className='experience__details'>
                    <GrMysql className='experience__details-icon'/>
                    <h4>MYSQL</h4>
                    <small className='text-light'>Intermediate</small>
                    </article>

                <article className='experience__details'>
                    <SiExpress className='experience__details-icon'/>
                    <h4>Express</h4>
                    <small className='text-light'>Beginner</small>
                </article>

              



            </div>

        </div>

    </div>
   </section>
  )
}

export default Experience