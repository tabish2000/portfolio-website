import React from 'react'
import './footer.css'
import{FaFacebookF} from 'react-icons/fa'
import{IoLogoTwitter} from 'react-icons/io'
import{FcReddit} from 'react-icons/fc'

function Footer() {
  return (
    <footer>
      <a href='#' className='footer__logo'>TABISH2K</a>

      <ul className='permalinks'>
        <li><a href ="#" >Home</a></li>
        <li><a href ="#about" >About</a></li>
        <li><a href ="#experience" >Experience</a></li>
        <li><a href ="#contact" >Contact</a></li>
      </ul>

      <div className='footer__socials'>
        <a href='https://facebook.com'><FaFacebookF/></a>
        <a href='https://twitter.com'><IoLogoTwitter/></a>
        <a href='https://www.reddit.com/user/retard-forever'><FcReddit/></a>
      </div>

      <div className='footer__copyright'>
        <small>All Rights Reserved</small>
      </div>
    </footer>
  )
}

export default Footer