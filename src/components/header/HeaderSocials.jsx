import React from 'react'
import{BsLinkedin} from 'react-icons/bs'
import {FaGitlab} from 'react-icons/fa'
import {ImStackoverflow} from 'react-icons/im'

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href='https://linkedin.com' target="_blank"><BsLinkedin/></a>
        <a href='https://gitlab.com' target="_blank"><FaGitlab/></a>
        <a href='stackoverflow.com' target="_blank"><ImStackoverflow/></a>
    </div>
  )
}

export default HeaderSocials