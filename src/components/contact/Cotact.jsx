import React from 'react'
import './contact.css'
import {MdOutlineEmail} from 'react-icons/md'
import { BsWhatsapp } from 'react-icons/bs'

function Cotact() {
  return (
    <section id="contact">
      <h5>Get In Touch</h5>
      <h2>Contact Me</h2>

      <div className='container contact__container'>
        <div className='contact__options'>

          <article className='contact__option'>
            <MdOutlineEmail className="contact__option-icon"/>
            <h4>Email</h4>
            <h5>ukayetabish142@gmail.com</h5>
            <a href='mailto:ukayetabish142@gmail.com' target='_blank'>Send me a message</a>
          </article>

          <article className='contact__option'>
            <BsWhatsapp className="contact__option-icon"/>
            <h4>WhatsApp</h4>
            
            <a href='https://api.whatsapp.com/send?phone=+919372652258' target='_blank'>Send me a message</a>
          </article>

        </div>
        {/* END OF CONTACT OPTIONS */}
        
        <form action=''>
          
          <input type="text" name="name" placeholder='Your Full Name' required></input>
          <input type="email" name="email" placeholder='Your Email' required></input>
          <textarea name="message" rows="7" placeholder='Your Message' required></textarea>
          <button type='Submit' className='btn btn-primary'>Send Message</button>

        </form>
      </div>

      </section>
  )
}

export default Cotact