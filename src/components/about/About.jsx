import React from 'react'
import './about.css'
import{BsPersonWorkspace} from 'react-icons/bs'
import{VscFolder} from 'react-icons/vsc'
import{TbCertificate} from 'react-icons/tb'
import ME from '../../assets/me-about.jpg'

function About() {
  return (
    <section id='about'>
            <h5>Get To Know</h5>
            <h2>About Me</h2>

            <div className='container about__container'>
                <div className='about__me'>
                  <div className='about__me-image'>
                    <img src={ME} alt="About Image"/>
                  </div>
                </div>
            

            <div className='about__content'>

              <div className='about__cards'>

                <article className='about__card'>
                  <BsPersonWorkspace className='about__icon'/>
                  <h5>Experience</h5>
                  <small>Freenlancing</small>
                </article>

                <article className='about__card'>
                  <VscFolder className='about__icon'/>
                  <h5>Projects</h5>
                  <small>5 Completed</small>
                </article>

                <article className='about__card'>
                  <TbCertificate className='about__icon'/>
                  <h5>Qualification</h5>
                  <small>BScIT Graduate</small>
                </article>

                {/* <article className='about__card'>
                  <TbCertificate className='about__icon'/>
                  <h5>Qualification</h5>
                  <small>BScIT Graduate</small>
                </article>

                <article className='about__card'>
                  <TbCertificate className='about__icon'/>
                  <h5>Qualification</h5>
                  <small>BScIT Graduate</small>
                </article>

                <article className='about__card'>
                  <TbCertificate className='about__icon'/>
                  <h5>Qualification</h5>
                  <small>BScIT Graduate</small>
                </article> */}

             
              
                

              </div>

             

            </div>
            </div>
    </section>
  )
}

export default About